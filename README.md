# OpenSEES_XML

#### 介绍

基于XML的结构性特征，通用解析OpenSEES分析结果。

#### 解决方案

1. OpenSEES_XML项目为反序列化OpenSEES结果文件的主入口；

2. XML_Test为OpenSEES_XML的测试项目，可预览OpenSEES_XML的对外方法；

3. OS_XML_Form为OpenSEES_XML基础上封装的实用exe项目，可查看并获取时程及滞回数据。

#### OpenSEES_XML基本架构

1. OpenSeesXML类为主入口，通过GetXMLResponse(string)可获得响应基类BasicXML；

2. BasicXML包含关键属性TimeList, TagList分别可获得时间序列及记录对象的编号列表；

3. BasicXML包含关键索引[int], [int, string]分别可获得响应对象及响应时程；

4. DriftXML, NodeXML, ElementXML为BasicXML的派生类；

5. DriftResponse, NodeResponse, ElementResponse, SectionResponse, FiberResponse为Response的派生类；

6. 可根据项目中的指引文件（.doc）添加未知的相应类型。

#### 依赖项

1. OpenSEES_XML: PGMHelper.dll

2. XML_Test: PGMHelper.dll OpenSEES.XML.dll

3. OS_XML_Form: PGMHelper.dll OpenSEES.XML.dll

#### 可执行文件

1. Release2019121

2. Release20200522：补充自定义滞回曲线绘图及数据统计功能

### Example

1. Beam: 零长度单元变形及单元力

2. Column: 纤维单元单元力、截面变形及材料应变

3. Global: 层间位移角、楼层位移、楼层速度、楼层加速度等

4. LayerShell: 分层壳相关分析结果

5. CyclicLoading: 构件低周往复加载

### 相关推送及链接

1. [OpenSEES实例集锦](https://gitee.com/civilwilson/OpenSEES_Examples)

2. [【工具】OpenSEES_XML分析结果提取工具[开源]](https://mp.weixin.qq.com/s/3Cheo5GtkfsDtFbohaVkdQ)

