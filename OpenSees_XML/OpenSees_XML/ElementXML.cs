﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 单元分析结果序列化容器
    /// </summary>
    [XmlRoot("OpenSees")]
    public class ElementXML : BasicXML
    {
        /// <summary>
        /// 单元响应
        /// </summary>
        [XmlElement("ElementOutput")]
        public List<ElementResponse> ElementOutputs { set; get; }

        /// <summary>
        /// 获得响应基类列表
        /// </summary>
        /// <returns></returns>
        public override List<Response> GetResponseList(bool includeTime)
        {
            //初始化
            var responseList = this.GetBasicResponseList(includeTime);
            //遍历单元
            this.ElementOutputs.ForEach(element =>
            {
                //获得响应
                var responses = element.GetResponses();
                //添加响应
                if (responses != null) responseList.AddRange(responses);
            });
            //返回值
            return responseList;
        }

        [XmlIgnore]
        /// <summary>
        /// 是否完整的分析结果
        /// </summary>
        /// <returns></returns>
        public override bool isObjectDeserialize
        {
            get
            {
                //是否存在单元
                if (this.ElementOutputs == null || this.ElementOutputs.Count == 0) return false;
                //获得分析结果类型
                foreach(var element  in this.ElementOutputs)
                {
                    if (!element.isObjectDeserialize) return false;
                }
                return true;
            }
        }
    }
}
