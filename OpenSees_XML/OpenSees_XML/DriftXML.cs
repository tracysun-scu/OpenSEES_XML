﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 层间位移角分析结果序列化容器
    /// </summary>
    [XmlRoot("OpenSees")]
    public class DriftXML : BasicXML
    {
        /// <summary>
        /// 层间位移角
        /// </summary>
        [XmlElement("DriftOutput")]
        public List<DriftResponse> DriftOutputs { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 结果是否完整
        /// </summary>
        /// <returns></returns>
        public override bool isObjectDeserialize
        {
            get
            {
                return this.DriftOutputs != null && this.DriftOutputs.Count != 0
                    && this.DriftOutputs.First().ResponseTypes != null;
            }
        }

        /// <summary>
        /// 获得响应基类列表
        /// </summary>
        /// <returns></returns>
        public override List<Response> GetResponseList(bool includeTime)
        {
            //初始化
            var responseList = this.GetBasicResponseList(includeTime);
            //遍历
            this.DriftOutputs.ForEach(response => responseList.Add(response));
            //返回值
            return responseList;
        }
    }
}
