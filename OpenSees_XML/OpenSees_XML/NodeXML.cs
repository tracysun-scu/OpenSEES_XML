﻿using PGMHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 节点分析结果序列化容器
    /// </summary>
    [XmlRoot("OpenSees")]
    public class NodeXML : BasicXML
    {
        /// <summary>
        /// 节点输出结果
        /// </summary>
        [XmlElement("NodeOutput")]
        public List<NodeResponse> NodeOutputs { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 结果是否完整
        /// </summary>
        /// <returns></returns>
        public override bool isObjectDeserialize
        {
            get
            {
                return this.NodeOutputs != null && this.NodeOutputs.Count != 0 &&
                    this.NodeOutputs.First().ResponseTypes != null;
            }
        }

        /// <summary>
        /// 获得响应基类列表
        /// </summary>
        /// <returns></returns>
        public override List<Response> GetResponseList(bool includeTime)
        {
            //初始化
            var responseList = this.GetBasicResponseList(includeTime);
            //遍历
            this.NodeOutputs.ForEach(response => responseList.Add(response));
            //返回值
            return responseList;
        }
    }
}
