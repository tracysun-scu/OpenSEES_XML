﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 纤维结果输出
    /// </summary>
    public class FiberXML
    {
        /// <summary>
        /// 纤维y坐标
        /// </summary>
        [XmlAttribute]
        public double yLoc { set; get; }

        /// <summary>
        /// 纤维z坐标
        /// </summary>
        [XmlAttribute]
        public double zLoc { set; get; }

        /// <summary>
        /// 纤维面积
        /// </summary>
        [XmlAttribute]
        public double area { set; get; }

        /// <summary>
        /// 纤维厚度
        /// </summary>
        [XmlAttribute]
        public double thickness { set; get; }

        /// <summary>
        /// 纤维序号
        /// </summary>
        [XmlAttribute]
        public double number { set; get; }

        /// <summary>
        /// 截面分析结果
        /// 不可以列表输出
        /// </summary>
        [XmlElement("UniaxialMaterialOutput")]
        public FiberResponse MaterialOut { set; get; }

        /// <summary>
        /// 纤维材料结果
        /// </summary>
        [XmlElement("NdMaterialOutput")]
        public FiberResponse NdMaterialOut { set; get; }

        /// <summary>
        /// 纤维是否合法
        /// </summary>
        /// <returns></returns>
        public bool isFiberValid()
        {
            return !this.MaterialOut.isEmpty;
        }

        /// <summary>
        /// 多维纤维是否合法
        /// </summary>
        /// <returns></returns>
        public bool isNdFiberValid()
        {
            return !this.NdMaterialOut.isEmpty;
        }

        /// <summary>
        /// 空的构造函数
        /// </summary>
        public FiberXML()
        {
            this.MaterialOut = new FiberResponse();
            this.NdMaterialOut = new FiberResponse();
        }
    }    

}
