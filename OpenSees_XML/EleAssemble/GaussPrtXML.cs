﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 高斯积分点
    /// </summary>
    public class GaussPrtXML
    {
        /// <summary>
        /// 高斯点位置
        /// </summary>
        [XmlAttribute]
        public int number { set; get; }

        /// <summary>
        /// 未知
        /// </summary>
        [XmlAttribute]
        public double eta { set; get; }

        /// <summary>
        /// 未知
        /// </summary>
        [XmlAttribute]
        public double neta { set; get; }

        /// <summary>
        /// 截面分析结果
        /// </summary>
        [XmlElement("SectionOutput")]
        public SectionResponse SectionOutput { set; get; }

        /// <summary>
        /// 分层壳截面分析结果
        /// </summary>
        [XmlElement("SectionForceDeformation")]
        public ShellSectionResponse SectionForceDeformation { set; get; }

        /// <summary>
        /// 高斯积分点分析结果
        /// </summary>
        [XmlElement("FiberOutput")]
        public FiberXML FiberOutput { set; get; }

        /// <summary>
        /// Truss单元的材料结果输出
        /// </summary>
        [XmlElement("UniaxialMaterialOutput")]
        public FiberResponse MaterialOut { set; get; }

        /// <summary>
        /// 设定单元编号
        /// </summary>
        /// <param name="eleTag"></param>
        public void SetElementTag(int eleTag)
        {
            //截面Tag
            this.SectionOutput.eleTag = eleTag;
            //获得纤维截面
            var response = this.GetFiberResponse();
            //截面存在
            if (response == null) return;
            //设定编号
            ((FiberResponse)response).eleTag = eleTag;        
        }

        /// <summary>
        /// 获得响应
        /// </summary>
        /// <returns></returns>
        public Response GetFiberResponse()
        {
            //纤维存在
            if (this.SectionOutput.FiberOutput.isFiberValid())
                return this.SectionOutput.FiberOutput.MaterialOut;
            //纤维存在
            else if (this.FiberOutput.isFiberValid())
                return this.FiberOutput.MaterialOut;
            else if (this.FiberOutput.isNdFiberValid())
                return this.FiberOutput.NdMaterialOut;
            else if (!this.MaterialOut.isEmpty)
                return this.MaterialOut;
            else return null;
        }

        /// <summary>
        /// 纤维截面是否合法
        /// </summary>
        /// <returns></returns>
        public bool isFiberSectionValid()
        {
            //获得纤维截面
            var response = this.GetFiberResponse();
            //截面存在
            if (response == null) return false;
            else return !((FiberResponse)response).isEmpty;
        }

        /// <summary>
        /// 空的构造函数
        /// </summary>
        public GaussPrtXML()
        {
            this.SectionOutput = new SectionResponse();
            this.SectionForceDeformation = new ShellSectionResponse();
            this.FiberOutput = new FiberXML();
            this.MaterialOut = new FiberResponse();
        }
    }
}
