﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 纤维分析结果
    /// </summary>
    public class FiberResponse : Response
    {
        /// <summary>
        /// 材料类型
        /// </summary>
        [XmlAttribute]
        public string matType { set; get; }

        /// <summary>
        /// 材料序号
        /// </summary>
        [XmlAttribute]
        public int matTag { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 单元编号
        /// </summary>
        public int eleTag { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 编号
        /// </summary>
        public override int ID
        {
            get
            {
                return this.eleTag;
            }
            set
            {

            }
        }

        /// <summary>
        /// 空的构造函数
        /// </summary>
        public FiberResponse()
        {
            this.matType = string.Empty;
        }
    }
}
