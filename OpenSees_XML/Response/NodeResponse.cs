﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 节点响应
    /// </summary>
    public class NodeResponse : Response
    {
        /// <summary>
        /// 节点编号
        /// </summary>
        [XmlAttribute]
        public int nodeTag { set; get; }

        /// <summary>
        /// X向节点坐标
        /// </summary>
        [XmlAttribute]
        public double coord1 { set; get; }

        /// <summary>
        /// Y向节点坐标
        /// </summary>
        [XmlAttribute]
        public double coord2 { set; get; }

        /// <summary>
        /// Z向节点坐标
        /// </summary>
        [XmlAttribute]
        public double coord3 { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 编号
        /// </summary>
        public override int ID
        {
            get
            {
                return this.nodeTag;
            }
            set
            {

            }
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        public NodeResponse()
        {

        }
    }
}
