﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

using PGMHelper;

namespace OpenSees.XML
{
    /// <summary>
    /// 响应基类
    /// </summary>
    public class Response
    {
        /// <summary>
        /// 响应类型
        /// </summary>
        [XmlElement("ResponseType")]
        public List<string> ResponseTypes { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 分析结果
        /// </summary>
        public List<List<float>> ResponseResult { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 响应类型数目
        /// </summary>
        /// <returns></returns>
        public int Count
        {
            get
            {
                return this.ResponseTypes.Count;
            }
        }

        [XmlIgnore]
        /// <summary>
        /// 获得响应
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        public List<float> this[string types]
        {
            get
            {
                //获得序号
                int index = this.ResponseTypes.IndexOf(types);
                //找不到对应序号
                return index == -1 ? new List<float> { 0F } : ResponseResult[index];
            }
        }

        [XmlIgnore]
        /// <summary>
        /// 是否为空
        /// </summary>
        public bool isEmpty
        {
            get
            {
                return this.ResponseTypes == null || this.ResponseTypes.Count == 0;
            }
        }

        [XmlIgnore]
        /// <summary>
        /// 标签描述
        /// </summary>
        public string Descp
        {
            get
            {
                return this.tagObj.Descp;
            }
            set
            {

            }
        }

        #region Virtial Properties 待重写的属性

        [XmlIgnore]
        /// <summary>
        /// 编号
        /// </summary>
        public virtual int ID
        {
            get
            {
                return 0;
            }
            set
            {

            }
        }

        [XmlIgnore]
        /// <summary>
        /// 标签对象
        /// </summary>
        public virtual TagObj tagObj
        {
            get
            {
                return new TagObj(this.ID, 0);
            }
        }

        #endregion

        /// <summary>
        /// 设定响应结果
        /// </summary>
        /// <param name="cellList"></param>
        /// <param name="cellIndex"></param>
        /// <returns></returns>
        public bool SetResponse(List<string> cellList, ref int cellIndex)
        {
            try
            {
                foreach (List<float> valueList in this.ResponseResult)
                {
                    valueList.Add(Convert.ToSingle(cellList[cellIndex]));
                    cellIndex++;
                }
                return true;
            }
            catch (Exception ex)
            {
                return MessageBoxExtern.Error(ex.Message);
            }
        }

        #region Constructs

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="responseTypeList"> 响应对象列表 </param>
        public Response(List<string> responseTypeList)
        {
            //初始化
            this.ResponseTypes = responseTypeList;
            initialResponseResult();
        }

        /// <summary>
        /// 无参数构造函数
        /// </summary>
        public Response()
        {
            // 初始化
            this.ResponseTypes = new List<string>();
            this.initialResponseResult();
        }

        /// <summary>
        /// 初始化响应结果
        /// </summary>
        public Response initialResponseResult()
        {
            //实例化
            this.ResponseResult = new List<List<float>>();
            //遍历
            for (int index = 0; index < ResponseTypes.Count; index++)
                this.ResponseResult.Add(new List<float>());

            //响应类型相同则修改
            int i = 2;
            for (int index=1; index < ResponseTypes.Count; index++)
            {
                if (ResponseTypes.Take(index).Contains(ResponseTypes[index]))
                {
                    ResponseTypes[index] = string.Format("{0}_{1}", ResponseTypes[index], i.ToString());
                    i++;
                }

            }
            return this;
        }

        #endregion
    }
}
