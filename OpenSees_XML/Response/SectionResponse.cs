﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 截面分析结果
    /// </summary>
    public class SectionResponse : Response
    {
        /// <summary>
        /// 截面类型
        /// </summary>
        [XmlAttribute]
        public string secType { set; get; }

        /// <summary>
        /// 截面编号
        /// </summary>
        [XmlAttribute]
        public int secTag { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 单元编号
        /// </summary>
        public int eleTag { set; get; }

        /// <summary>
        /// 高斯积分点分析结果
        /// </summary>
        [XmlElement("FiberOutput")]
        public FiberXML FiberOutput { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 编号
        /// </summary>
        public override int ID
        {
            get
            {
                return this.eleTag;
            }
            set
            {

            }
        }

        /// <summary>
        /// 空的构造函数
        /// </summary>
        public SectionResponse()
        {
            this.secType = string.Empty;
            this.FiberOutput = new XML.FiberXML();
        }
    }

}
