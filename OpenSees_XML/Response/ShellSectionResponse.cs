﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 截面分析结果
    /// </summary>
    public class ShellSectionResponse : Response
    {
        /// <summary>
        /// 截面类型
        /// </summary>
        [XmlAttribute]
        public string classType { set; get; }

        /// <summary>
        /// 截面编号
        /// </summary>
        [XmlAttribute]
        public int tag { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 单元编号
        /// </summary>
        public int eleTag { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 子编号（高斯点编号）
        /// </summary>
        public int GaussPointID { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 编号
        /// </summary>
        public override int ID
        {
            get
            {
                return this.eleTag;
            }
            set
            {

            }
        }

        [XmlIgnore]
        /// <summary>
        /// 标签对象
        /// </summary>
        public override TagObj tagObj
        {
            get
            {
                return new TagObj(this.ID, this.GaussPointID);
            }
        }

        /// <summary>
        /// 空的构造函数
        /// </summary>
        public ShellSectionResponse()
        {
            this.classType = string.Empty;
        }
    }

}
