﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OpenSees.XML
{
    /// <summary>
    /// 层间位移角响应
    /// </summary>
    public class DriftResponse : Response
    {
        /// <summary>
        /// i端节点
        /// </summary>
        [XmlAttribute]
        public int node1 { set; get; }

        /// <summary>
        /// j端节点
        /// </summary>
        [XmlAttribute]
        public int node2 { set; get; }

        /// <summary>
        /// 节点间向量长度
        /// </summary>
        [XmlAttribute]
        public double lengthPerpDirn { set; get; }

        [XmlIgnore]
        /// <summary>
        /// 编号
        /// </summary>
        public override int ID
        {
            get
            {
                return this.node1;
            }
            set
            {
                
            }
        }
    }
}
