﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenSees.XML;
using PGMHelper;
using System.Windows.Forms.DataVisualization.Charting;

namespace OS_XML_Form
{
    public partial class MainForm : Form
    {
        #region XML基本对象

        /// <summary>
        /// X轴分析结果
        /// </summary>
        private BasicXML XAxisXML { set; get; }

        /// <summary>
        /// Y轴分析结果
        /// </summary>
        private BasicXML YAxisXML { set; get; }

        /// <summary>
        /// 时程分析结果
        /// </summary>
        private BasicXML TimeHistXML { set; get; }

        /// <summary>
        /// 临时XML变量
        /// </summary>
        private BasicXML TemporaryXML { set; get; }

        /// <summary>
        /// 单元分析结果
        /// </summary>
        private ElementDataSet ElementDatas { set; get; }

        #endregion

        /// <summary>
        /// 构造函数
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            //TabControl外观
            this.HysteresisTabControl.SetApperance();
            //Chart外观
            this.HysteresisChart.SetApperance(StringAlignment.Center, false);
            //Chart标题
            this.HysteresisChart.AddTitles("Hysteresis Loops", string.Empty, string.Empty);
            this.HysteresisChart.Titles[2].TextOrientation = TextOrientation.Rotated270;
            //科学记数
            this.HysteresisChart.SetAxisFormat(ChartAxisType.AxisX, AxisFormat.Science, 1, 1);
            this.HysteresisChart.SetAxisFormat(ChartAxisType.AxisY, AxisFormat.Science, 1, 1);
            //GridView外观
            this.HysteresisDataGridView.SetApperance( DataGridViewSelectionMode.ColumnHeaderSelect);
            //控件禁用
            this.SetElementControlEnable(false);
            this.SetHistControlEnable(false);
            this.SetUserDefineControlEnable(false);
        }

        #region 选择XML文件

        /// <summary>
        /// 选择XML文件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XMLSelectButtonClick(object sender, EventArgs e)
        {
            //获得控件
            var button = (Button)sender;
            //判断类型
            if (button.Name.Contains("TimeHist"))
            {
                this.TimeHistXML = this.GetXMLObject(button, "Select XML File") ? this.TemporaryXML : null;
                //刷新控件
                this.RefreshTagCheckListbox();
            }
            //HysteresisXAxis
            else if (button.Name.Contains("ElemetXAxis"))
            {
                this.XAxisXML = this.GetXMLObject(button, "X Axis") ? this.TemporaryXML : null;
                this.RefreshElementTagCheckListbox();
            }
            else if (button.Name.Contains("ElemetYAxis"))
            {
                this.YAxisXML = this.GetXMLObject(button, "Y Axis") ? this.TemporaryXML : null;
                this.RefreshElementTagCheckListbox();
            }
            //UserDefineXAxis
            else if (button.Name.Contains("UserDefineXAxis"))
            {
                this.XAxisXML = this.GetXMLObject(button, "X Axis") ? this.TemporaryXML : null;
                this.RefreshUserDefineListbox();
            }
            else if (button.Name.Contains("UserDefineYAxis"))
            {
                this.YAxisXML = this.GetXMLObject(button, "Y Axis") ? this.TemporaryXML : null;
                this.RefreshUserDefineListbox();
            }
        }

        /// <summary>
        /// 获取XML对象
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="button"></param>
        /// <param name="orginText"></param>
        private bool GetXMLObject(Button button, string orginText)
        {
            //修改文字
            button.Text = orginText;
            //文件路径
            string filePath = string.Empty;
            //选择文件
            if (!WilsonHelper.OpenFile("XML(*.xml)|*.xml|文本文件(*.txt)|*.txt|所有文件(*.*)|*.*", ref filePath))
                return false;
            //获得结果
            this.TemporaryXML = OpenSeesXML.GetXMLResponse(filePath);
            //是否成功
            if (!this.TemporaryXML.isSuccess) return MessageBoxExtern.Error("Something Wrong!");
            //判断是否为单元对象
            if(this.TemporaryXML is ElementXML && orginText.Contains("select"))
                return MessageBoxExtern.Error("Only Element Object Available!");
            //成功修改按钮文字
            button.Text = "OK";
            return true;
        }

        #endregion

        #region 刷新控件

        /// <summary>
        /// 刷新单元编号列表
        /// </summary>
        private void RefreshElementTagCheckListbox()
        {
            //控件禁用
            this.SetElementControlEnable(false);
            //避免值为空
            if (this.XAxisXML == null || this.YAxisXML == null) return;
            if (!(this.XAxisXML is ElementXML) || !(this.YAxisXML is ElementXML)) return;
            //获得单元组分析结果
            this.ElementDatas = new ElementDataSet((ElementXML)this.XAxisXML, (ElementXML)this.YAxisXML);
            //数据绑定
            this.ElementTagCheckListbox.SetDataSource(this.ElementDatas.Datas, "Descp", "Descp");
            //若无单元符合返回
            if (this.ElementDatas.Count == 0)
            {
                MessageBoxExtern.Error("NO Same Element Tag."); return;
            }
            //更新ConboBox
            this.RefreshComboBox(this.XAxisComboBox, this.XAxisXML.GetResponseTypeList(false));
            this.RefreshComboBox(this.YAxisComboBox, this.YAxisXML.GetResponseTypeList(false));
            //全选
            this.ElementTagCheckListbox.CheckedAll();
            //控件开启
            this.SetElementControlEnable(true);
        }

        /// <summary>
        /// 刷新编号列表
        /// </summary>
        private void RefreshTagCheckListbox()
        {
            //控件禁用
            this.SetHistControlEnable(false);
            //避免值为空
            if (this.TimeHistXML == null) return;
            //刷新
            this.TagChecklistbox.SetDataSource(this.TimeHistXML.ResponseList, "Descp", "Descp");
            //更新ConboBox
            this.RefreshComboBox(this.ResponseComboBox, this.TimeHistXML.GetResponseTypeList(false));
            //全选
            this.TagChecklistbox.CheckedAll();
            //控件开启
            this.SetHistControlEnable(true);
        }

        /// <summary>
        /// 刷新用户定义的列表
        /// </summary>
        private void RefreshUserDefineListbox()
        {
            //控件禁用
            this.SetUserDefineControlEnable(false);
            //避免值为空
            if(this.XAxisXML != null)
            {
                //数据绑定
                this.XAxisListbox.SetDataSource(this.XAxisXML.ResponseList, "Descp", "Descp");
                this.RefreshComboBox(this.UserDefineXAxisResponseTypeComboBox, this.XAxisXML.GetResponseTypeList(false));
            }
            if(this.YAxisXML != null)
            {
                this.YAxisListbox.SetDataSource(this.YAxisXML.ResponseList, "Descp", "Descp");
                this.RefreshComboBox(this.UserDefineYAxisResponseTypeComboBox, this.YAxisXML.GetResponseTypeList(false));
            }
            //是否同时不为空
            if (this.XAxisXML == null || this.YAxisXML == null) return;
            //控件开启
            this.SetUserDefineControlEnable(true);
        }

        /// <summary>
        /// 单元控件是否开启
        /// </summary>
        /// <param name="isEnable"></param>
        private bool SetElementControlEnable(bool isEnable)
        {
            this.ElementTagGroupBox.Enabled = isEnable;
            this.HysteresisResponseGroupbox.Enabled = isEnable;
            this.HysteresisPlotButton.Enabled = isEnable;
            return isEnable;
        }

        /// <summary>
        /// 时程相关控件是否开启
        /// </summary>
        /// <param name="isEnable"></param>
        /// <returns></returns>
        private bool SetHistControlEnable(bool isEnable)
        {
            this.TagGroupbox.Enabled = isEnable;
            this.TimeHistResponseGroupbox.Enabled = isEnable;
            this.TimeHistPlotButton.Enabled = isEnable;
            return isEnable;
        }

        /// <summary>
        /// 用户定义的控件
        /// </summary>
        /// <param name="isEnable"></param>
        /// <returns></returns>
        private bool SetUserDefineControlEnable(bool isEnable)
        {
            this.UserDefineXAixsGroupbox.Enabled = isEnable;
            this.UserDefineYAixsGroupbox.Enabled = isEnable;
            this.UserDefinePlotButton.Enabled = isEnable;
            return isEnable;
        }

        /// <summary>
        /// 修改下拉菜单栏
        /// </summary>
        /// <param name="box"></param>
        /// <param name="Response_list"></param>
        private void RefreshComboBox(ComboBox box ,List<string> Response_list)
        {
            //初始化
            box.Text = string.Empty;
            box.Items.Clear();
            //响应为空
            if (Response_list.Count == 0) return;
            //遍历
            Response_list.ForEach(response => box.Items.Add(response));
            //问题替换
            box.Text = box.Items[0].ToString();
        }

        /// <summary>
        /// 改变坐标轴文字
        /// </summary>
        /// <param name="titleList"></param>
        private void SetChartTitleText(List<string> txtList)
        {
            //判断数目是否相符
            if (txtList.Count != 3) return;
            //设定坐标轴文字
            this.HysteresisChart.SetTitle(TiltleType.Header, txtList[0]);
            this.HysteresisChart.SetTitle(TiltleType.AxisX, txtList[1]);
            this.HysteresisChart.SetTitle(TiltleType.AxisY, txtList[2]);
        }

        #endregion

        /// <summary>
        /// 时间响应时程
        /// </summary>
        /// <param name="descp"></param>
        /// <returns></returns>
        public GridViewDatas TimeHistData(string descp)
        {
            //初始化
            var data = new GridViewDatas("Time History");
            //获得时间列表
            var timeList = this.TimeHistXML.TimeList;
            //加入第一列时间
            data.Add(new GridViewColumn(timeList, demical: 2, titleName: "时间"));
            //获得勾选的对象
            var responseList = this.TagChecklistbox.GetObjects().ConvertAll(r => (Response)r);
            //遍历响应添加值
            responseList.ForEach(response =>
            {
                //获得响应列表
                var valueList = response[descp];
                //判断值是否相同
                if (valueList.Count == timeList.Count)
                {
                    data.Add(new GridViewColumn(valueList, demical: 10, format: "E2", 
                        titleName: response.Descp));
                    //创建序列
                    this.HysteresisChart.AddSeries(response.Descp, SeriesChartType.Line, 2);
                    //添加绘图数据
                    this.HysteresisChart.Series[response.Descp].Points.DataBindXY(timeList, valueList);
                }
            });
            //返回结果
            return data;
        }

        /// <summary>
        /// 滞回曲线
        /// </summary>
        /// <param name="xDescp"></param>
        /// <param name="yDescp"></param>
        /// <returns></returns>
        public GridViewDatas HystereticData(string xDescp, string yDescp)
        {
            //初始化
            var dataList = new GridViewDatas("Hysteresis Loops");
            //获得勾选对象
            var elementList = this.ElementTagCheckListbox.GetObjects().ConvertAll(e => (ElementData)e);
            //遍历单元对象添加值
            elementList.ForEach(element =>
            {
                //获得列表
                var data = element.Data(xDescp, yDescp);
                //列表是否为空
                if(data.Count != 0)
                {
                    //创建序列
                    this.HysteresisChart.AddSeries(data.Name, SeriesChartType.Line, 2);
                    //添加绘图数据
                    this.HysteresisChart.Series[data.Name].Points.DataBindXY(
                        data[0].GetValues().ConvertAll(v => Convert.ToSingle(v)), 
                        data[1].GetValues().ConvertAll(v => Convert.ToSingle(v)));
                    //添加列表
                    dataList.AddRange(data);
                }
            });
            //返回结果
            return dataList;
        }

        /// <summary>
        /// 用户自定义
        /// </summary>
        /// <param name="xDescp"></param>
        /// <param name="yDescp"></param>
        /// <returns></returns>
        public GridViewDatas UserDefineData(string xDescp, string yDescp)
        {
            //初始化
            var data = new GridViewDatas(string.Format("{0}-{1} RelationShip", xDescp, yDescp));
            //获得勾选对象
            var xResponse = (Response)this.XAxisListbox.GetSelectObject();
            var yResponse = (Response)this.YAxisListbox.GetSelectObject();
            //获得响应列表
            var xResponseList = xResponse[xDescp];
            var yResponseList = yResponse[yDescp];
            //判断数据是否一致
            if (xResponseList.Count != yResponseList.Count)
            {
                MessageBoxExtern.Warnning("数据量不一致!");
                return data;
            }
            //加入第一列
            data.Add(new GridViewColumn(xResponseList, demical: 6, titleName: xDescp));
            //加入第二列
            data.Add(new GridViewColumn(yResponseList, demical: 6, titleName: yDescp));
            //创建序列
            this.HysteresisChart.AddSeries("RelationShip", SeriesChartType.Line, Color.Red,3);
            //添加绘图数据
            this.HysteresisChart.Series["RelationShip"].Points.DataBindXY(xResponseList, yResponseList);
            return data;
        }

        /// <summary>
        /// 全选和反选功能
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckListBoxEvent(object sender, EventArgs e)
        {
            //获得控件
            var button = (Control)sender;
            //根据名称判断类型
            if(button.Name.Contains("Time"))
            {
                if(button.Name.Contains("All")) this.TagChecklistbox.CheckedAll();
                else this.TagChecklistbox.InverseChecked();
            }
            else if(button.Name.Contains("Hys"))
            {
                if (button.Name.Contains("All")) this.ElementTagCheckListbox.CheckedAll();
                else this.ElementTagCheckListbox.InverseChecked();
            }
        }

        /// <summary>
        /// 绘图按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlotButtonClick(object sender, EventArgs e)
        {
            //清空Chart
            this.HysteresisChart.Series.Clear();
            //初始化
            var data = new GridViewDatas();
            //获得控件
            var button = (Control)sender;
            //根据名称判断类型
            if (button.Name.Contains("Time"))
            {
                //图例可见性
                this.HysteresisChart.Legends[0].Enabled = true;
                //获得数据
                data = this.TimeHistData(this.ResponseComboBox.Text);
                //设定坐标轴文字
                this.SetChartTitleText(new List<string> { "Time History",
                this.TimeHistXML.isTimeEmpty? "": "Time",
                this.ResponseComboBox.Text });
            }
            else if (button.Name.Contains("Hys"))
            {
                //图例可见性
                this.HysteresisChart.Legends[0].Enabled = true;
                //获得滞回曲线数据
                data = this.HystereticData(this.XAxisComboBox.Text, this.YAxisComboBox.Text);
                //设定坐标轴文字
                this.SetChartTitleText(new List<string> { "Hysteresis Loops",
                this.XAxisComboBox.Text, this.YAxisComboBox.Text});
            }
            else if (button.Name.Contains("User"))
            {
                //图例可见性
                this.HysteresisChart.Legends[0].Enabled = false;
                //获得滞回曲线数据
                data = this.UserDefineData(this.UserDefineXAxisResponseTypeComboBox.Text, this.UserDefineYAxisResponseTypeComboBox.Text);
                //获得描述
                var XDescp = this.UserDefineXAxisResponseTypeComboBox.Text;
                var YDescp = this.UserDefineYAxisResponseTypeComboBox.Text;
                //设定坐标轴文字
                this.SetChartTitleText(new List<string> { string.Format("{0}-{1} RelationShip", YDescp, XDescp), XDescp, YDescp });
            }
            //DATAGIRDVIEW
            this.HysteresisDataGridView.AddDatas(data);
        }

        /// <summary>
        /// 帮助
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpForm.Show(new List<string> { "OpenSEES XML Helper",
                "OpenSEES分析结果处理助手",
                "https://gitee.com/civilwilson/OpenSEES_XML",
            "20200522"}, Properties.Resources.LOGO);
        }
    }
}
