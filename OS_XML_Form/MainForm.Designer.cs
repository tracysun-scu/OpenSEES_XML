﻿namespace OS_XML_Form
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.HysteresisResponseGroupbox = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.XAxisComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.YAxisComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ElemetXAxisSelectButton = new System.Windows.Forms.Button();
            this.ElemetYAxisSelectButton = new System.Windows.Forms.Button();
            this.HysteresisPlotButton = new System.Windows.Forms.Button();
            this.ElementTagGroupBox = new System.Windows.Forms.GroupBox();
            this.Hysteresis_Select_Inverse_Button = new System.Windows.Forms.Button();
            this.Hystersis_Select_All_Button = new System.Windows.Forms.Button();
            this.ElementTagCheckListbox = new System.Windows.Forms.CheckedListBox();
            this.HysteresisTabControl = new System.Windows.Forms.TabControl();
            this.Hysteresis_Graph = new System.Windows.Forms.TabPage();
            this.HysteresisChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Hysteresis_Digital = new System.Windows.Forms.TabPage();
            this.HysteresisDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.TimeHistPlotButton = new System.Windows.Forms.Button();
            this.TimeHistResponseGroupbox = new System.Windows.Forms.GroupBox();
            this.ResponseComboBox = new System.Windows.Forms.ComboBox();
            this.TagGroupbox = new System.Windows.Forms.GroupBox();
            this.TimeHist_Select_Inverse_Button = new System.Windows.Forms.Button();
            this.TagChecklistbox = new System.Windows.Forms.CheckedListBox();
            this.TimeHist_Select_All_Button = new System.Windows.Forms.Button();
            this.TimeHistSelectButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UserDefineGroupbox = new System.Windows.Forms.GroupBox();
            this.UserDefineYAixsGroupbox = new System.Windows.Forms.GroupBox();
            this.UserDefineYResponseTypeGroupbox = new System.Windows.Forms.GroupBox();
            this.UserDefineYAxisResponseTypeComboBox = new System.Windows.Forms.ComboBox();
            this.YAxisListbox = new System.Windows.Forms.ListBox();
            this.UserDefineYAxisButton = new System.Windows.Forms.Button();
            this.UserDefinePlotButton = new System.Windows.Forms.Button();
            this.UserDefineXAixsGroupbox = new System.Windows.Forms.GroupBox();
            this.XAxisListbox = new System.Windows.Forms.ListBox();
            this.UserDefineXResponseTypeGroupbox = new System.Windows.Forms.GroupBox();
            this.UserDefineXAxisResponseTypeComboBox = new System.Windows.Forms.ComboBox();
            this.UserDefineXAxisButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.HysteresisResponseGroupbox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.ElementTagGroupBox.SuspendLayout();
            this.HysteresisTabControl.SuspendLayout();
            this.Hysteresis_Graph.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HysteresisChart)).BeginInit();
            this.Hysteresis_Digital.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HysteresisDataGridView)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.TimeHistResponseGroupbox.SuspendLayout();
            this.TagGroupbox.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.UserDefineGroupbox.SuspendLayout();
            this.UserDefineYAixsGroupbox.SuspendLayout();
            this.UserDefineYResponseTypeGroupbox.SuspendLayout();
            this.UserDefineXAixsGroupbox.SuspendLayout();
            this.UserDefineXResponseTypeGroupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.HysteresisResponseGroupbox);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.HysteresisPlotButton);
            this.groupBox1.Controls.Add(this.ElementTagGroupBox);
            this.groupBox1.Location = new System.Drawing.Point(175, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(158, 555);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hysteresis Loops";
            // 
            // HysteresisResponseGroupbox
            // 
            this.HysteresisResponseGroupbox.Controls.Add(this.groupBox3);
            this.HysteresisResponseGroupbox.Controls.Add(this.groupBox4);
            this.HysteresisResponseGroupbox.Location = new System.Drawing.Point(6, 351);
            this.HysteresisResponseGroupbox.Name = "HysteresisResponseGroupbox";
            this.HysteresisResponseGroupbox.Size = new System.Drawing.Size(140, 134);
            this.HysteresisResponseGroupbox.TabIndex = 1;
            this.HysteresisResponseGroupbox.TabStop = false;
            this.HysteresisResponseGroupbox.Text = "Response_Type";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.XAxisComboBox);
            this.groupBox3.Location = new System.Drawing.Point(6, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(126, 50);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "X_Axis";
            // 
            // XAxisComboBox
            // 
            this.XAxisComboBox.FormattingEnabled = true;
            this.XAxisComboBox.Location = new System.Drawing.Point(7, 21);
            this.XAxisComboBox.Name = "XAxisComboBox";
            this.XAxisComboBox.Size = new System.Drawing.Size(113, 20);
            this.XAxisComboBox.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.YAxisComboBox);
            this.groupBox4.Location = new System.Drawing.Point(7, 76);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(125, 50);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Y_Axis";
            // 
            // YAxisComboBox
            // 
            this.YAxisComboBox.FormattingEnabled = true;
            this.YAxisComboBox.Location = new System.Drawing.Point(7, 21);
            this.YAxisComboBox.Name = "YAxisComboBox";
            this.YAxisComboBox.Size = new System.Drawing.Size(112, 20);
            this.YAxisComboBox.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ElemetXAxisSelectButton);
            this.groupBox6.Controls.Add(this.ElemetYAxisSelectButton);
            this.groupBox6.Location = new System.Drawing.Point(6, 21);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(140, 59);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Select XML File";
            // 
            // ElemetXAxisSelectButton
            // 
            this.ElemetXAxisSelectButton.Location = new System.Drawing.Point(6, 20);
            this.ElemetXAxisSelectButton.Name = "ElemetXAxisSelectButton";
            this.ElemetXAxisSelectButton.Size = new System.Drawing.Size(60, 33);
            this.ElemetXAxisSelectButton.TabIndex = 0;
            this.ElemetXAxisSelectButton.Text = "X Axis";
            this.ElemetXAxisSelectButton.UseVisualStyleBackColor = true;
            this.ElemetXAxisSelectButton.Click += new System.EventHandler(this.XMLSelectButtonClick);
            // 
            // ElemetYAxisSelectButton
            // 
            this.ElemetYAxisSelectButton.Location = new System.Drawing.Point(74, 20);
            this.ElemetYAxisSelectButton.Name = "ElemetYAxisSelectButton";
            this.ElemetYAxisSelectButton.Size = new System.Drawing.Size(60, 33);
            this.ElemetYAxisSelectButton.TabIndex = 1;
            this.ElemetYAxisSelectButton.Text = "Y Axis";
            this.ElemetYAxisSelectButton.UseVisualStyleBackColor = true;
            this.ElemetYAxisSelectButton.Click += new System.EventHandler(this.XMLSelectButtonClick);
            // 
            // HysteresisPlotButton
            // 
            this.HysteresisPlotButton.Location = new System.Drawing.Point(6, 491);
            this.HysteresisPlotButton.Name = "HysteresisPlotButton";
            this.HysteresisPlotButton.Size = new System.Drawing.Size(140, 50);
            this.HysteresisPlotButton.TabIndex = 6;
            this.HysteresisPlotButton.Text = "Plot";
            this.HysteresisPlotButton.UseVisualStyleBackColor = true;
            this.HysteresisPlotButton.Click += new System.EventHandler(this.PlotButtonClick);
            // 
            // ElementTagGroupBox
            // 
            this.ElementTagGroupBox.Controls.Add(this.Hysteresis_Select_Inverse_Button);
            this.ElementTagGroupBox.Controls.Add(this.Hystersis_Select_All_Button);
            this.ElementTagGroupBox.Controls.Add(this.ElementTagCheckListbox);
            this.ElementTagGroupBox.Location = new System.Drawing.Point(6, 86);
            this.ElementTagGroupBox.Name = "ElementTagGroupBox";
            this.ElementTagGroupBox.Size = new System.Drawing.Size(140, 259);
            this.ElementTagGroupBox.TabIndex = 2;
            this.ElementTagGroupBox.TabStop = false;
            this.ElementTagGroupBox.Text = "Element Tags";
            // 
            // Hysteresis_Select_Inverse_Button
            // 
            this.Hysteresis_Select_Inverse_Button.Location = new System.Drawing.Point(76, 223);
            this.Hysteresis_Select_Inverse_Button.Name = "Hysteresis_Select_Inverse_Button";
            this.Hysteresis_Select_Inverse_Button.Size = new System.Drawing.Size(58, 30);
            this.Hysteresis_Select_Inverse_Button.TabIndex = 3;
            this.Hysteresis_Select_Inverse_Button.Text = "Inverse";
            this.Hysteresis_Select_Inverse_Button.UseVisualStyleBackColor = true;
            this.Hysteresis_Select_Inverse_Button.Click += new System.EventHandler(this.CheckListBoxEvent);
            // 
            // Hystersis_Select_All_Button
            // 
            this.Hystersis_Select_All_Button.Location = new System.Drawing.Point(7, 223);
            this.Hystersis_Select_All_Button.Name = "Hystersis_Select_All_Button";
            this.Hystersis_Select_All_Button.Size = new System.Drawing.Size(63, 30);
            this.Hystersis_Select_All_Button.TabIndex = 2;
            this.Hystersis_Select_All_Button.Text = "All";
            this.Hystersis_Select_All_Button.UseVisualStyleBackColor = true;
            this.Hystersis_Select_All_Button.Click += new System.EventHandler(this.CheckListBoxEvent);
            // 
            // ElementTagCheckListbox
            // 
            this.ElementTagCheckListbox.CheckOnClick = true;
            this.ElementTagCheckListbox.FormattingEnabled = true;
            this.ElementTagCheckListbox.Location = new System.Drawing.Point(7, 21);
            this.ElementTagCheckListbox.Name = "ElementTagCheckListbox";
            this.ElementTagCheckListbox.Size = new System.Drawing.Size(127, 196);
            this.ElementTagCheckListbox.TabIndex = 0;
            // 
            // HysteresisTabControl
            // 
            this.HysteresisTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HysteresisTabControl.Controls.Add(this.Hysteresis_Graph);
            this.HysteresisTabControl.Controls.Add(this.Hysteresis_Digital);
            this.HysteresisTabControl.Location = new System.Drawing.Point(6, 18);
            this.HysteresisTabControl.Name = "HysteresisTabControl";
            this.HysteresisTabControl.SelectedIndex = 0;
            this.HysteresisTabControl.Size = new System.Drawing.Size(628, 523);
            this.HysteresisTabControl.TabIndex = 5;
            // 
            // Hysteresis_Graph
            // 
            this.Hysteresis_Graph.Controls.Add(this.HysteresisChart);
            this.Hysteresis_Graph.Location = new System.Drawing.Point(4, 22);
            this.Hysteresis_Graph.Name = "Hysteresis_Graph";
            this.Hysteresis_Graph.Padding = new System.Windows.Forms.Padding(3);
            this.Hysteresis_Graph.Size = new System.Drawing.Size(620, 497);
            this.Hysteresis_Graph.TabIndex = 0;
            this.Hysteresis_Graph.Text = "Graph";
            this.Hysteresis_Graph.UseVisualStyleBackColor = true;
            // 
            // HysteresisChart
            // 
            this.HysteresisChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.HysteresisChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.HysteresisChart.Legends.Add(legend1);
            this.HysteresisChart.Location = new System.Drawing.Point(7, 7);
            this.HysteresisChart.Name = "HysteresisChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.HysteresisChart.Series.Add(series1);
            this.HysteresisChart.Size = new System.Drawing.Size(607, 481);
            this.HysteresisChart.TabIndex = 0;
            this.HysteresisChart.Text = "chart1";
            // 
            // Hysteresis_Digital
            // 
            this.Hysteresis_Digital.Controls.Add(this.HysteresisDataGridView);
            this.Hysteresis_Digital.Location = new System.Drawing.Point(4, 22);
            this.Hysteresis_Digital.Name = "Hysteresis_Digital";
            this.Hysteresis_Digital.Padding = new System.Windows.Forms.Padding(3);
            this.Hysteresis_Digital.Size = new System.Drawing.Size(620, 497);
            this.Hysteresis_Digital.TabIndex = 1;
            this.Hysteresis_Digital.Text = "Digital";
            this.Hysteresis_Digital.UseVisualStyleBackColor = true;
            // 
            // HysteresisDataGridView
            // 
            this.HysteresisDataGridView.AllowUserToDeleteRows = false;
            this.HysteresisDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HysteresisDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HysteresisDataGridView.Location = new System.Drawing.Point(7, 7);
            this.HysteresisDataGridView.Name = "HysteresisDataGridView";
            this.HysteresisDataGridView.RowTemplate.Height = 23;
            this.HysteresisDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.ColumnHeaderSelect;
            this.HysteresisDataGridView.Size = new System.Drawing.Size(607, 484);
            this.HysteresisDataGridView.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.HysteresisTabControl);
            this.groupBox7.Location = new System.Drawing.Point(604, 28);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(637, 554);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "DATA";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.TimeHistPlotButton);
            this.groupBox8.Controls.Add(this.TimeHistResponseGroupbox);
            this.groupBox8.Controls.Add(this.TagGroupbox);
            this.groupBox8.Controls.Add(this.TimeHistSelectButton);
            this.groupBox8.Location = new System.Drawing.Point(12, 28);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(157, 555);
            this.groupBox8.TabIndex = 8;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Time History";
            // 
            // TimeHistPlotButton
            // 
            this.TimeHistPlotButton.Location = new System.Drawing.Point(6, 491);
            this.TimeHistPlotButton.Name = "TimeHistPlotButton";
            this.TimeHistPlotButton.Size = new System.Drawing.Size(140, 50);
            this.TimeHistPlotButton.TabIndex = 7;
            this.TimeHistPlotButton.Text = "Plot";
            this.TimeHistPlotButton.UseVisualStyleBackColor = true;
            this.TimeHistPlotButton.Click += new System.EventHandler(this.PlotButtonClick);
            // 
            // TimeHistResponseGroupbox
            // 
            this.TimeHistResponseGroupbox.Controls.Add(this.ResponseComboBox);
            this.TimeHistResponseGroupbox.Location = new System.Drawing.Point(7, 436);
            this.TimeHistResponseGroupbox.Name = "TimeHistResponseGroupbox";
            this.TimeHistResponseGroupbox.Size = new System.Drawing.Size(140, 49);
            this.TimeHistResponseGroupbox.TabIndex = 4;
            this.TimeHistResponseGroupbox.TabStop = false;
            this.TimeHistResponseGroupbox.Text = "Response_Type";
            // 
            // ResponseComboBox
            // 
            this.ResponseComboBox.FormattingEnabled = true;
            this.ResponseComboBox.Location = new System.Drawing.Point(6, 20);
            this.ResponseComboBox.Name = "ResponseComboBox";
            this.ResponseComboBox.Size = new System.Drawing.Size(128, 20);
            this.ResponseComboBox.TabIndex = 9;
            // 
            // TagGroupbox
            // 
            this.TagGroupbox.Controls.Add(this.TimeHist_Select_Inverse_Button);
            this.TagGroupbox.Controls.Add(this.TagChecklistbox);
            this.TagGroupbox.Controls.Add(this.TimeHist_Select_All_Button);
            this.TagGroupbox.Location = new System.Drawing.Point(7, 60);
            this.TagGroupbox.Name = "TagGroupbox";
            this.TagGroupbox.Size = new System.Drawing.Size(139, 370);
            this.TagGroupbox.TabIndex = 3;
            this.TagGroupbox.TabStop = false;
            this.TagGroupbox.Text = "Tags";
            // 
            // TimeHist_Select_Inverse_Button
            // 
            this.TimeHist_Select_Inverse_Button.Location = new System.Drawing.Point(75, 331);
            this.TimeHist_Select_Inverse_Button.Name = "TimeHist_Select_Inverse_Button";
            this.TimeHist_Select_Inverse_Button.Size = new System.Drawing.Size(58, 30);
            this.TimeHist_Select_Inverse_Button.TabIndex = 5;
            this.TimeHist_Select_Inverse_Button.Text = "Inverse";
            this.TimeHist_Select_Inverse_Button.UseVisualStyleBackColor = true;
            this.TimeHist_Select_Inverse_Button.Click += new System.EventHandler(this.CheckListBoxEvent);
            // 
            // TagChecklistbox
            // 
            this.TagChecklistbox.CheckOnClick = true;
            this.TagChecklistbox.FormattingEnabled = true;
            this.TagChecklistbox.Location = new System.Drawing.Point(6, 20);
            this.TagChecklistbox.Name = "TagChecklistbox";
            this.TagChecklistbox.Size = new System.Drawing.Size(127, 308);
            this.TagChecklistbox.TabIndex = 4;
            // 
            // TimeHist_Select_All_Button
            // 
            this.TimeHist_Select_All_Button.Location = new System.Drawing.Point(6, 331);
            this.TimeHist_Select_All_Button.Name = "TimeHist_Select_All_Button";
            this.TimeHist_Select_All_Button.Size = new System.Drawing.Size(63, 30);
            this.TimeHist_Select_All_Button.TabIndex = 4;
            this.TimeHist_Select_All_Button.Text = "All";
            this.TimeHist_Select_All_Button.UseVisualStyleBackColor = true;
            this.TimeHist_Select_All_Button.Click += new System.EventHandler(this.CheckListBoxEvent);
            // 
            // TimeHistSelectButton
            // 
            this.TimeHistSelectButton.Location = new System.Drawing.Point(6, 20);
            this.TimeHistSelectButton.Name = "TimeHistSelectButton";
            this.TimeHistSelectButton.Size = new System.Drawing.Size(140, 33);
            this.TimeHistSelectButton.TabIndex = 2;
            this.TimeHistSelectButton.Text = "Select XML File";
            this.TimeHistSelectButton.UseVisualStyleBackColor = true;
            this.TimeHistSelectButton.Click += new System.EventHandler(this.XMLSelectButtonClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1250, 25);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(47, 21);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.aboutToolStripMenuItem.Text = "About OSXML";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // UserDefineGroupbox
            // 
            this.UserDefineGroupbox.Controls.Add(this.UserDefineYAixsGroupbox);
            this.UserDefineGroupbox.Controls.Add(this.UserDefineYAxisButton);
            this.UserDefineGroupbox.Controls.Add(this.UserDefinePlotButton);
            this.UserDefineGroupbox.Controls.Add(this.UserDefineXAixsGroupbox);
            this.UserDefineGroupbox.Controls.Add(this.UserDefineXAxisButton);
            this.UserDefineGroupbox.Location = new System.Drawing.Point(339, 28);
            this.UserDefineGroupbox.Name = "UserDefineGroupbox";
            this.UserDefineGroupbox.Size = new System.Drawing.Size(259, 555);
            this.UserDefineGroupbox.TabIndex = 10;
            this.UserDefineGroupbox.TabStop = false;
            this.UserDefineGroupbox.Text = "UserDefine";
            // 
            // UserDefineYAixsGroupbox
            // 
            this.UserDefineYAixsGroupbox.Controls.Add(this.UserDefineYResponseTypeGroupbox);
            this.UserDefineYAixsGroupbox.Controls.Add(this.YAxisListbox);
            this.UserDefineYAixsGroupbox.Location = new System.Drawing.Point(136, 60);
            this.UserDefineYAixsGroupbox.Name = "UserDefineYAixsGroupbox";
            this.UserDefineYAixsGroupbox.Size = new System.Drawing.Size(117, 425);
            this.UserDefineYAixsGroupbox.TabIndex = 9;
            this.UserDefineYAixsGroupbox.TabStop = false;
            this.UserDefineYAixsGroupbox.Text = "Y Axis Tags";
            // 
            // UserDefineYResponseTypeGroupbox
            // 
            this.UserDefineYResponseTypeGroupbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserDefineYResponseTypeGroupbox.Controls.Add(this.UserDefineYAxisResponseTypeComboBox);
            this.UserDefineYResponseTypeGroupbox.Location = new System.Drawing.Point(6, 374);
            this.UserDefineYResponseTypeGroupbox.Name = "UserDefineYResponseTypeGroupbox";
            this.UserDefineYResponseTypeGroupbox.Size = new System.Drawing.Size(100, 45);
            this.UserDefineYResponseTypeGroupbox.TabIndex = 10;
            this.UserDefineYResponseTypeGroupbox.TabStop = false;
            this.UserDefineYResponseTypeGroupbox.Text = "Response_Type";
            // 
            // UserDefineYAxisResponseTypeComboBox
            // 
            this.UserDefineYAxisResponseTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserDefineYAxisResponseTypeComboBox.FormattingEnabled = true;
            this.UserDefineYAxisResponseTypeComboBox.Location = new System.Drawing.Point(6, 20);
            this.UserDefineYAxisResponseTypeComboBox.Name = "UserDefineYAxisResponseTypeComboBox";
            this.UserDefineYAxisResponseTypeComboBox.Size = new System.Drawing.Size(88, 20);
            this.UserDefineYAxisResponseTypeComboBox.TabIndex = 9;
            // 
            // YAxisListbox
            // 
            this.YAxisListbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.YAxisListbox.FormattingEnabled = true;
            this.YAxisListbox.ItemHeight = 12;
            this.YAxisListbox.Location = new System.Drawing.Point(7, 21);
            this.YAxisListbox.Name = "YAxisListbox";
            this.YAxisListbox.Size = new System.Drawing.Size(104, 340);
            this.YAxisListbox.TabIndex = 6;
            // 
            // UserDefineYAxisButton
            // 
            this.UserDefineYAxisButton.Location = new System.Drawing.Point(136, 21);
            this.UserDefineYAxisButton.Name = "UserDefineYAxisButton";
            this.UserDefineYAxisButton.Size = new System.Drawing.Size(117, 33);
            this.UserDefineYAxisButton.TabIndex = 8;
            this.UserDefineYAxisButton.Text = "Y Axis";
            this.UserDefineYAxisButton.UseVisualStyleBackColor = true;
            this.UserDefineYAxisButton.Click += new System.EventHandler(this.XMLSelectButtonClick);
            // 
            // UserDefinePlotButton
            // 
            this.UserDefinePlotButton.Location = new System.Drawing.Point(6, 491);
            this.UserDefinePlotButton.Name = "UserDefinePlotButton";
            this.UserDefinePlotButton.Size = new System.Drawing.Size(247, 50);
            this.UserDefinePlotButton.TabIndex = 7;
            this.UserDefinePlotButton.Text = "Plot";
            this.UserDefinePlotButton.UseVisualStyleBackColor = true;
            this.UserDefinePlotButton.Click += new System.EventHandler(this.PlotButtonClick);
            // 
            // UserDefineXAixsGroupbox
            // 
            this.UserDefineXAixsGroupbox.Controls.Add(this.XAxisListbox);
            this.UserDefineXAixsGroupbox.Controls.Add(this.UserDefineXResponseTypeGroupbox);
            this.UserDefineXAixsGroupbox.Location = new System.Drawing.Point(7, 60);
            this.UserDefineXAixsGroupbox.Name = "UserDefineXAixsGroupbox";
            this.UserDefineXAixsGroupbox.Size = new System.Drawing.Size(123, 425);
            this.UserDefineXAixsGroupbox.TabIndex = 3;
            this.UserDefineXAixsGroupbox.TabStop = false;
            this.UserDefineXAixsGroupbox.Text = "X Axis";
            // 
            // XAxisListbox
            // 
            this.XAxisListbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.XAxisListbox.FormattingEnabled = true;
            this.XAxisListbox.ItemHeight = 12;
            this.XAxisListbox.Location = new System.Drawing.Point(7, 21);
            this.XAxisListbox.Name = "XAxisListbox";
            this.XAxisListbox.Size = new System.Drawing.Size(110, 340);
            this.XAxisListbox.TabIndex = 6;
            // 
            // UserDefineXResponseTypeGroupbox
            // 
            this.UserDefineXResponseTypeGroupbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserDefineXResponseTypeGroupbox.Controls.Add(this.UserDefineXAxisResponseTypeComboBox);
            this.UserDefineXResponseTypeGroupbox.Location = new System.Drawing.Point(7, 374);
            this.UserDefineXResponseTypeGroupbox.Name = "UserDefineXResponseTypeGroupbox";
            this.UserDefineXResponseTypeGroupbox.Size = new System.Drawing.Size(110, 45);
            this.UserDefineXResponseTypeGroupbox.TabIndex = 4;
            this.UserDefineXResponseTypeGroupbox.TabStop = false;
            this.UserDefineXResponseTypeGroupbox.Text = "Response_Type";
            // 
            // UserDefineXAxisResponseTypeComboBox
            // 
            this.UserDefineXAxisResponseTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UserDefineXAxisResponseTypeComboBox.FormattingEnabled = true;
            this.UserDefineXAxisResponseTypeComboBox.Location = new System.Drawing.Point(6, 20);
            this.UserDefineXAxisResponseTypeComboBox.Name = "UserDefineXAxisResponseTypeComboBox";
            this.UserDefineXAxisResponseTypeComboBox.Size = new System.Drawing.Size(98, 20);
            this.UserDefineXAxisResponseTypeComboBox.TabIndex = 9;
            // 
            // UserDefineXAxisButton
            // 
            this.UserDefineXAxisButton.Location = new System.Drawing.Point(6, 20);
            this.UserDefineXAxisButton.Name = "UserDefineXAxisButton";
            this.UserDefineXAxisButton.Size = new System.Drawing.Size(124, 33);
            this.UserDefineXAxisButton.TabIndex = 2;
            this.UserDefineXAxisButton.Text = "X Axis";
            this.UserDefineXAxisButton.UseVisualStyleBackColor = true;
            this.UserDefineXAxisButton.Click += new System.EventHandler(this.XMLSelectButtonClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1250, 588);
            this.Controls.Add(this.UserDefineGroupbox);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OpenSEES XML";
            this.groupBox1.ResumeLayout(false);
            this.HysteresisResponseGroupbox.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.ElementTagGroupBox.ResumeLayout(false);
            this.HysteresisTabControl.ResumeLayout(false);
            this.Hysteresis_Graph.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HysteresisChart)).EndInit();
            this.Hysteresis_Digital.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HysteresisDataGridView)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.TimeHistResponseGroupbox.ResumeLayout(false);
            this.TagGroupbox.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.UserDefineGroupbox.ResumeLayout(false);
            this.UserDefineYAixsGroupbox.ResumeLayout(false);
            this.UserDefineYResponseTypeGroupbox.ResumeLayout(false);
            this.UserDefineXAixsGroupbox.ResumeLayout(false);
            this.UserDefineXResponseTypeGroupbox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ElemetYAxisSelectButton;
        private System.Windows.Forms.Button ElemetXAxisSelectButton;
        private System.Windows.Forms.GroupBox ElementTagGroupBox;
        private System.Windows.Forms.CheckedListBox ElementTagCheckListbox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox YAxisComboBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox XAxisComboBox;
        private System.Windows.Forms.Button HysteresisPlotButton;
        private System.Windows.Forms.TabControl HysteresisTabControl;
        private System.Windows.Forms.TabPage Hysteresis_Graph;
        private System.Windows.Forms.TabPage Hysteresis_Digital;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox HysteresisResponseGroupbox;
        private System.Windows.Forms.DataVisualization.Charting.Chart HysteresisChart;
        private System.Windows.Forms.Button Hysteresis_Select_Inverse_Button;
        private System.Windows.Forms.Button Hystersis_Select_All_Button;
        private System.Windows.Forms.DataGridView HysteresisDataGridView;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button TimeHistPlotButton;
        private System.Windows.Forms.GroupBox TimeHistResponseGroupbox;
        private System.Windows.Forms.ComboBox ResponseComboBox;
        private System.Windows.Forms.GroupBox TagGroupbox;
        private System.Windows.Forms.Button TimeHist_Select_Inverse_Button;
        private System.Windows.Forms.CheckedListBox TagChecklistbox;
        private System.Windows.Forms.Button TimeHist_Select_All_Button;
        private System.Windows.Forms.Button TimeHistSelectButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox UserDefineGroupbox;
        private System.Windows.Forms.Button UserDefineYAxisButton;
        private System.Windows.Forms.Button UserDefinePlotButton;
        private System.Windows.Forms.GroupBox UserDefineXResponseTypeGroupbox;
        private System.Windows.Forms.ComboBox UserDefineXAxisResponseTypeComboBox;
        private System.Windows.Forms.GroupBox UserDefineXAixsGroupbox;
        private System.Windows.Forms.ListBox XAxisListbox;
        private System.Windows.Forms.Button UserDefineXAxisButton;
        private System.Windows.Forms.GroupBox UserDefineYAixsGroupbox;
        private System.Windows.Forms.GroupBox UserDefineYResponseTypeGroupbox;
        private System.Windows.Forms.ComboBox UserDefineYAxisResponseTypeComboBox;
        private System.Windows.Forms.ListBox YAxisListbox;
    }
}

