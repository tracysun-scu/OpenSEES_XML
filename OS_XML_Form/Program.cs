﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
//using Log_in;

namespace OS_XML_Form
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            //窗体基本设定
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ////窗体打开时获得Token
            //Log_In.Form_Load_Get_Token();
            ////检验登录情况
            //if (Log_In.Log_In_State_Check() == false)
            //{
            //    MessageBox.Show("未登录", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            Application.Run(new MainForm());
        }
    }
}
