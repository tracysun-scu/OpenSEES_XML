﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using PGMHelper;
using System.Xml.Serialization;
using System.IO;
using OpenSees.XML;

namespace XML_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 测试按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //文件路径
            string filePath = string.Empty;
            //选择文件
            if (!WilsonHelper.OpenFile("文本文件(*.txt)|*.txt|所有文件(*.*)|*.*", ref filePath)) return;
            //传入文件路径完成XML解析
            BasicXML result = OpenSeesXML.GetXMLResponse(filePath);
            //分析结果是否解析成功
            if (!result.isSuccess) return;
            //获得响应类型列表
            var responseType = result.GetResponseTypeList(false);
            //获得对象编号列表
            var tagList = result.TagList;
            //根据对象编号及响应类型获得分析结果
            List<float> response = result[tagList.First(), responseType.First()];
            //是否存在时间序列(返回布尔值)
            var isTimeEmpty = result.isTimeEmpty;
            //获得时间序列
            var timeList = result.TimeList;
            //带判断的移除List前几个元素
            if (!RemoveFontIndex(ref timeList, 5))
            {

            }

        }

        /// <summary>
        /// 移开List的前几个元素
        /// </summary>
        /// <param name="valueList"></param>
        /// <param name="removeNum"></param>
        /// <returns></returns>
        private bool RemoveFontIndex(ref List<float> valueList, int removeNum)
        {
            try
            {
                for (int i = 1; i <= removeNum; i++)
                    valueList.RemoveAt(0);
                return true;
            }
            catch { return false; }
        }
    }
   
}

